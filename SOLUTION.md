# Problem

First, let's note that, unlike the `unassign` handler, the `assign` handler doesn't have any checks as to whether the employee has already been assigned to a channel before attempting to assign it.
Could it be that the UI of one player isn't updated in time, and is issuing the command to assign an already assigned employee? However, before exploring this path and adding the check, let's consider the issues in the `unassign` handler (that does have such a check) closer.

I added one log line in the server.ts after the check:

```
...
const assignment = 
    await db.collection("assignments").findOne({
        _id: new ObjectId(assignmentId),
      });

if (!assignment) return;

console.log("Assignment found, preparing to delete it");
```

Now, if we click the "unassign" button several times quickly (from one or different browser tabs), we'll see that the line `"Assignment found, preparing to delete it"` is printed multiple times, illustrating that multiple requests are able to succesfully go through the check. This means that each of the requests will attempt to delete the assignment from the database and (unconditionally) decrement the marketing points of that channel, leading to incorrect results. 

Obviously, this behavior is so easy to reproduce due to the `sleep` right after the check, as none of the requests are able to complete and actually delete the assignment before another request starts and performs the same check. However, even without the `sleep`, if multiple concurrent requests are possible, such outcomes will always happen once in a while, leading to intermittent issues (that is consistent with what the users are describing).

The same would apply when assigning the employees to channels: even if we first check whether the employee is free, they can always become occupied right before we actually try to assign them. This would of course lead to incorrect results (consider the drawing).

![Step sequence](./images/drawing.png "Step sequence")

The fact that `Node.js` runtime is single-threaded does not really change anything: requests are still executed concurrently, and while one request is waiting for some IO (API call, database access, etc.), the main thread can be performing computations for a different request. In addition, when considering specific solutions, we need to remember that often such request handler code can be deployed on multiple nodes, executing truly in parallel.

# Solutions

To avoid such issues, when concurrently processing requests that consist of several steps, we need to make sure that all possible orderings of steps are valid and will not lead to inconsistent results.

There are several possible approaches to achieve this:

- Compose handler logic using various atomic operations that are provided by the database and designed to support concurrent scenarios. 
- Use various locks to make other requests wait if one request has already started updating the data.
- Store requests in a queue and process them sequentially, similar to what is used in CSP / actor model. 

Of course, all these approaches have their advantages and disadvantages. In this solution, I would like to focus on the first alternative, mainly because it requires minimal efforts to fix the mentioned problems. However, at the end I would also like to suggest a draft to illustrate the last alternative, and briefly discuss the scenarios when it may become necessary.

## Atomic updates in MongoDB

This solution attempts to solve the concurrency issues solely using MongoDB queries and is implemented in this merge request:
https://gitlab.com/pkapustin/simulation/-/merge_requests/2

- When it comes to deleting assignments, we make use of the fact that `deleteOne` returns 0 if the document is already deleted. In this case, we don't need to update the client or decrement the marketing channel counter (as it wasn't this request that deleted the assignment).

```
const { deletedCount } = await db.collection("assignments").deleteOne({
    _id: assignment._id,
  });

if (deletedCount == 0) return;
```

- When it comes to creating new assignments, the situation is a little bit more complex. One possibility is to use `updateOne`, combined with `upsert: true` and `$setOnInsert`. This allows us to create a new assignment only if it doesn't exist, and otherwise do nothing (in one atomic operation). Afterwards, we need to check the `matchedCount` value returned by `updateOne`. If it is more than 0, it means the assignment existed from before, and our request didn't change anything, in other words, we don't need to update the client and decrement the marketing channel counter.

```
const { matchedCount, upsertedId } =  await db.collection("assignments").updateOne(
  {
    employeeId: new ObjectId(employeeId)
  },
  { $setOnInsert: { channel } },
  {
    upsert: true
  }
);

if (matchedCount > 0) return;
```
One caveat is that we need to create a unique index on the `Assignment` document for the `employeeId` field, otherwise a situation when multiple Assignment documents would be created is still possible, see https://www.mongodb.com/docs/manual/reference/method/db.collection.findAndModify/#upsert-with-unique-index.

A different possible alternative would be to use a regular `insertOne` and handle the exception if the insert fails due to the unique index. For some reason I haven't thought about this while first working on the solution, and haven't tested it yet. A minor downside of this approach is more exceptions, but if this works, I think it would be a better approach due to its simplicity.

Note: we could also simplify the second query if we changed the data model a bit, for example, removing the `Assignment` document altogether and storing the assignment directly on the `Employee` document as an optional field. However, this appraoch seems a bit unfortunate from the data modeling perspective, as assignments really only concern marketing employees. 

Note: the merge request is currently missing any tests, I haven't had the chance to add them yet due to the lack of time.

## Sequential request processing

The motivation to consider a different approach is partly the fact that our queries are getting more complex, and we need to be very careful with details. Also, it is not easy to write tests for our logic as it is no longer expressed directly, but rather spread between different DB primitives.

Also, what if at some point we are not able to find a suitable primitive to accommodate the needs of the requirements? For example, what if there comes a requirement that at no point more than 5 employees can be assigned to one marketing channel? Or something even more complex? To accommodate these, we may have no choice but to tweak our data model, add various locks to the system and complicate its design in other ways. 

With more complex requirements there is a larger chance of different concurrency-related errors, possibly we miss a race condition or use locks incorrectly and create a dead-lock situation. These things can be very difficult to reproduce, debug, reason about and could slow down the development significantly.

One approach that allows to simplify things a lot on the conceptual level is sequential processing of the requests, related to CSP / actor model.
For example, in our case, we could create a separate process, or actor, that is responsible for assignment of employees. There would only be one instance of such process running at a time for one company.

Advantages of such approach:
- The logic remains very simple and can be easily unit tested
- There is basically no chance for concurrency-related issues as the requests are processed sequentially

Disadvantages of such approach:
- Some initial development cost to set up a separate process / actor
- Some requests may need to be redesigned as they no longer can return immediate response to the client (however, `ask` pattern from actor model can be used, so one can await a response from the process / actor)
- Performance must be considered so that sequential processing of the requests does not become a bottle-neck (however, one can usually "shard" these, running one process / actor per company or similar)
- When deployed, one still needs some sort of distributed lock to make sure there is only one process active at a time (however, this is just one lock that needs to be implemented correctly compared to an unknown amount of ad-hoc locks that might be necessary otherwise).

I have created a very simple draft to illustrate how sequential processing could help solve the original concurrency issues in this merge request: https://gitlab.com/pkapustin/simulation/-/merge_requests/1

This implementation is based on a trick, only for demonstration purposes, when a resolved promise is used as a queue (new tasks to run are "added" to the queue using `then()`).
https://stackoverflow.com/questions/65571719/nodejs-fully-executing-websocket-responses-sequentially-with-async-calls

This way, when the requests are processed, the handler actions, instead of being executed directly, are added to the request queue:

```
let requestQueue = Promise.resolve();

function enqueue(f: () => void): void {
  requestQueue = requestQueue.then(f);
}

...

socket.on("assign", async args => enqueue(() => (handleAssign(args))));
socket.on("unassign", async args => enqueue(() => handleUnassign(args)));

```
Of course, this solution isn't going to handle errors correctly (because only `then()` callback is provided), and all the other practical issues of the CSP / actor model that were mentioned above would need to be solved too.
However, the solution does work in the sense that for the "happy path" the requests are processed sequentially and correctly, and the mentioned concurrency issues don't happen.

# Other improvements
I did a couple of other small changes in the merge request:
https://gitlab.com/pkapustin/simulation/-/merge_requests/2

- Why is the `channel` field optional on the `Assignment`? I made this one required.
- I have created helper bindings for the MongoDB collections to reduce duplication and improve type safety, as MongoDB queries now return `Employee` and `Assignment` objects rather than `Document`:

```
const employeeCol = db.collection<Employee>("employees")
const assignmentCol = db.collection<Assignment>("assignments")
const productCol = db.collection<Product>("product")
```

# Known issues

## "Unassign <Could not find employee>" after server restart

There is currently one issue with server restarts that can be reproduced like this: 
- Open the UI
- Restart the server
- Assign some employees in the UI
- Reload the UI

This sequence of steps leads to "Unassign <Could not find employee>" being displayed on the buttons, because non-existing employees are assigned in the third after the DB is recreated in the second step.
One improvement that should definitely be made here is to validate that the employees that the UI is trying to assign actually exist in the database, this check is currently missing.

## Better feedback in the UI

Currently, there are situations in which the feedback to the user is suboptimal. For example, if two users click "Assign" button for two different channels at the same time, for one of the users it is going to appear as if no one was assigned to that channel.
I see two possible approaches to improve this:
- Change the web socket messages to closer match the UI, so that the message is saying "assign a free employee to the channel" rather than "assign a specific employee to the channel". With this approach, both user actions can complete without conflicts. 
- Change the UI to closer match the web socket messages (so that the users see they are attempting to assign a concrete employee to a channel), and display an error message if this specific assignment didn't work.
I would probably start with the first option as for the purposes of the simulation the students don't really care which exact employee is assigned to the channel.

I haven't yet tried implementing any of these improvements mostly due to the lack of time.
